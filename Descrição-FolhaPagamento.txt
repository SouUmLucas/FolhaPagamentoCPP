Exerc�cio: Folha de Pagamento

Uma empresa deseja gerenciar a folha de pagamento dos seus funcion�rios.

Em uma aplica��o de folha de pagamento anterior, voc� implementou 3 classes:
Empresa (cnpj, razaoSocial, nomeFantasia), Departamento (nome) e Funcionario (cpf, nome, salario).
O sal�rio do funcion�rio era armazenado em um atributo da classe Funcionario.

No entanto, nesta nova aplica��o, o sal�rio do funcion�rio dever� ser calculado pela aplica��o. Ser�o dois tipos de funcion�rios: horistas e mensalistas. Os horistas trabalham uma determinada quantidade de horas por m�s e recebem um valor X por hora. O sal�rio deles � igual a horasTrabalhadas * valorHora. Os mensalistas t�m um cargo (ex: analista, programador etc.) e, dependendo do cargo, ele recebe um sal�rio espec�fico (ex: programadores recebem 5000, analistas recebem 6000) etc.

Escreva o c�digo da aplica��o nova (e da antiga), assumindo as seguintes classes:
Empresa (cnpj, razaoSocial, nomeFantasia) - m�todos: calcularSalarioMedio
Departamento (nome) - m�todos: calcularSalarioMedio
Funcionario (cpf, nome, salario)
FuncionarioHorista (horasTrabalhas, valorHora) herda de Funcionario - m�todos: getSalario sobrescrito
FuncionarioMensalista (cargo) herda de funcionario - m�todos: getSalario sobrescrito