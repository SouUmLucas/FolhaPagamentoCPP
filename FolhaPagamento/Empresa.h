#include "Util.h"
#include "departamento.h"

class Empresa {
private:
	// atributos privados
	string cnpj = "",
		razao_social = "",
		nome_fantasia = "";

	/* Struct para conter dos objetos de uma lista de Departamentos */
	typedef struct tagDEPARTAMENTO {
		Departamento departamento;
		tagDEPARTAMENTO *proximoDepartamento;
	} DEPARTAMENTO;

	DEPARTAMENTO *ptrPrimeiro,
		*ptrUltimo,
		*stDepartamento;
public:
	// inicializa o objeto com o ponteiro para
	// o primeiro departamento nulo
	Empresa(void) {
		ptrPrimeiro = NULL;
	}

	// metodos publicos para manipulacao dos atributos
	// da class Empresa
	void set_cnpj(string cnpj) {
		Empresa::cnpj = cnpj;
	}

	string get_cnpj(void) {
		return Empresa::cnpj;
	}

	void set_razao_social(string razao_social) {
		Empresa::razao_social = razao_social;
	}

	string get_razao_social(void) {
		return Empresa::razao_social;
	}

	void set_nome_fantasia(string nome_fantasia) {
		Empresa::nome_fantasia = nome_fantasia;
	}

	string get_nome_fantasia(void) {
		return Empresa::nome_fantasia;
	}

	bool adicionar_departamento(Departamento *departamento) {
		if (ptrPrimeiro == NULL)
			ptrUltimo = ptrPrimeiro = new DEPARTAMENTO;
		else
			ptrUltimo = ptrUltimo->proximoDepartamento = new DEPARTAMENTO;

		// verifica se nao houve memoria disponivel
		if (!ptrUltimo)
			return false;

		// copia a referencia do objeto do novo departamento
		memcpy(ptrUltimo, departamento, sizeof(DEPARTAMENTO));
		ptrUltimo->proximoDepartamento = NULL;

		return true;
	}
};
