#include "empresa.h"

void Teste();

void main(void) {
	// Antes da liberacao, utilizar a funcao de teste
	//Teste();
	//

	Empresa emp_super_empresa;
	Departamento dep_desenvolvimento;
	FuncionarioHorista fn_lucas;
	FuncionarioMensalista fn_pedro;

	// Configura uma empresa
	emp_super_empresa.set_cnpj("132456789");
	emp_super_empresa.set_razao_social("Empresa Ltda.");
	emp_super_empresa.set_nome_fantasia("SuperEmpresa");
	
	// Configura um departamento
	dep_desenvolvimento.set_nome("Desenvolvimento");

	// Configura um funcionario horista
	fn_lucas.set_cpf("123456");
	fn_lucas.set_nome("Lucas Santos");
	fn_lucas.set_valor_hora(10.48);
	fn_lucas.set_horas_trabalhadas(40);

	// Configura um funcionario mensalista;
	fn_pedro.set_cpf("654321");
	fn_pedro.set_nome("Pedro Junior");
	fn_pedro.set_tipo_funcionario(EnumTipoFuncionario::GERENTE);

	// Associa um funcionario a um departamento
	dep_desenvolvimento.adicionario_funcionario(&fn_lucas);
	dep_desenvolvimento.adicionario_funcionario(&fn_pedro);

	dep_desenvolvimento.listar_funcionarios();
	// Associa um departamento a uma empresa
	emp_super_empresa.adicionar_departamento(&dep_desenvolvimento);
}

void Teste() {
}