#include "Util.h"
#include "funcionario_horista.h"
#include "funcionario_mensalista.h"

class Departamento {
private:
	string nome;

	typedef struct tagFUNCIONARIO {
		Funcionario funcionario;
		tagFUNCIONARIO *proximoFuncionario;
	} FUNCIONARIO;

	FUNCIONARIO *ptrPrimeiro,
		*ptrUltimo,
		*stFuncionario;
public:
	// inicializa o objeto com o ponteiro para o primeiro
	// funcionario nulo;
	Departamento(void) {
		ptrPrimeiro = NULL;
	}

	void set_nome(string nome) {
		Departamento::nome = nome;
	}

	string get_nome(void) {
		return Departamento::nome;
	}

	bool adicionario_funcionario(Funcionario *funcionario) {
		if (ptrPrimeiro == NULL)
			ptrUltimo = ptrPrimeiro = new FUNCIONARIO;
		else
			ptrUltimo = ptrUltimo->proximoFuncionario = new FUNCIONARIO;

		// verifica se houve memoria disponivel
		if (!ptrUltimo)
			return false;

		// copia a referencia do novo objeto de Funcionario
		memcpy(ptrUltimo, funcionario, sizeof(FUNCIONARIO));
		ptrUltimo->proximoFuncionario = NULL;

		return true;
	}

	// METODO TESTE APENAS PARA CHECAR A SOBRESCRITA
	void listar_funcionarios(void) {
		cout << "Listando funcionarios..." << endl;
		FUNCIONARIO *funcionario_atual;
		for (funcionario_atual = ptrPrimeiro; funcionario_atual; funcionario_atual = funcionario_atual->proximoFuncionario) {
			cout << "Nome: " << funcionario_atual->funcionario.get_nome() << endl
				<< "CPF: " << funcionario_atual->funcionario.get_cpf() << endl
				<< "Salario: " << funcionario_atual->funcionario.get_salario() << endl << endl;
		}
		cout << "Fim da listagem" << endl;
	}
};
