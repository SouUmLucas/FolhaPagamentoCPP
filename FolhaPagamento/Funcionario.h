#include "Util.h"

class Funcionario {
private:
	string cpf = "",
		nome = "";

	double salario = 0.0;

public:
	void set_cpf(string cpf) {
		Funcionario::cpf = cpf;
	}

	string get_cpf() {
		return Funcionario::cpf;
	}

	void set_nome(string nome) {
		Funcionario::nome = nome;
	}

	string get_nome() {
		return Funcionario::nome;
	}

	void set_salario(double salario) {
		Funcionario::salario = salario;
	}

	virtual double get_salario() {
		return Funcionario::salario;
	}
};