#include "Util.h"
#include "funcionario.h"

class FuncionarioHorista : public Funcionario {
private:
	int horas_trabalhadas;
	double valor_hora;

public:
	FuncionarioHorista() {
		horas_trabalhadas = 0;
		valor_hora = 0.0;
	}

	void FuncionarioHorista::set_horas_trabalhadas(int horas_trabalhadas) {
		FuncionarioHorista::horas_trabalhadas = horas_trabalhadas;
	}

	int get_horas_trabalhadas() {
		return FuncionarioHorista::horas_trabalhadas;
	}

	void FuncionarioHorista::set_valor_hora(double valor_hora) {
		FuncionarioHorista::valor_hora = valor_hora;
	}

	double get_valor_hora() {
		return FuncionarioHorista::valor_hora;
	}

	// metodo sobrescrito da classe Funcionario
	double get_salario() {
		return FuncionarioHorista::horas_trabalhadas * FuncionarioHorista::valor_hora;
	}
};